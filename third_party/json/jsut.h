#ifndef JSON_UTILITY_H
#define JSON_UTILITY_H

#include <array>
#include <vector>

namespace ju {
	template <typename TJson, typename TData>
	void from_json(const TJson& j, TData& data) {
		data = j.template get<TData>();
	}

	template <typename TJson, typename TData, size_t N>
	void from_json(const TJson& j, std::array<TData, N>& data) {
		for (auto i = 0; i < N; ++i) { from_json(j[i], data[i]); }
	}

	template <typename TJson, typename TData>
	void from_json(const TJson& j, std::vector<TData>& data) {
		const auto n = static_cast<int>(j.size());
		data.resize(n);
		for (auto i = 0; i < n; ++i) { from_json(j[i], data[i]); }
	}
	
	template <typename TJson>
	void parse_json(const TJson& /*j*/) {}

	template <typename TJson, typename TData, typename... Args>
	void parse_json(const TJson& j, const std::string& name,
		TData& data, Args&... args)
	{
		from_json(j[name], data);
		parse_json(j, args...);
	}

	template <typename TJson>
	void make_json(TJson& /*j*/) {}

	template <typename TJson, typename TData, typename... Args>
	void make_json(TJson& j, const std::string& name,
		const TData& data, const Args&... args)
	{
		j[name] = data;
		make_json(j, args...);
	}

} // namespace ju

#endif // JSON_UTILITY_H