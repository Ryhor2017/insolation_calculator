//
// Created by ryhor on 17/07/19.
//

#ifndef INSOLATION_CALCULATOR_MAKE_BEAM_RANGE_H
#define INSOLATION_CALCULATOR_MAKE_BEAM_RANGE_H


#include <array>
#include <vector>

using p3 = std::array<double, 3>;
std::vector<p3> make_beam_range(const double latitude, const int day, const int hour_bins_count);

#endif //INSOLATION_CALCULATOR_MAKE_BEAM_RANGE_H
