//
// Created by ryhor on 7/4/18.
//

#include <assert.h>
#include <chrono>
#include "solver.h"

namespace {

    int
    ray_is_passing_through_build(const int &omit_build_ind, const int &segm_numb, const sun::Point2d &point,
                                 const double &point_height,
                                 const double &tg_altitude,
                                 const sun::Vector2d &dir_to_sun,
                                 const std::vector<sun::PolygonByPoints> &building,
                                 const std::vector<double> &building_height,
                                 const sun::Segment &segm, const bool is_neighb) {

        if (!building.size()) {
            return 1;
        }
        int polyg_ind = -1;
        for (auto &polyg: building) {
            polyg_ind++;
            for (int i = 0; i < polyg.points_.size(); ++i) {
                int i_next = (i + 1) % polyg.points_.size();
                optional<sun::Point2d> inters_point =
                        sun::segments_intersection(segm.p1_, segm.p2_, polyg.points_[i], polyg.points_[i_next]);
                if (inters_point) {
                    if (!is_neighb) {
                        if (sun::distance(inters_point.value(), point) < sun::kMinDistBetwSectPoints) {
                            if (omit_build_ind == polyg_ind) {
                                continue;
                            } else {
                                return -1; // return closed windows_x
                            }
                        }
                    }
                    const double height_b = building_height.at(polyg_ind);
                    const double delta = sun::distance(point, inters_point.value()) * tg_altitude;
                    if (point_height + delta < height_b) {
                        return 0;
                    }
                }
            }
        }

        return 1;
    }

    int
    ray_is_passing(const int &omit_build_ind, const int &segm_numb, const sun::Point2d &point,
                   const double &point_height,
                   const double &tg_altitude,
                   const sun::Vector2d &dir_to_sun,
                   const std::vector<sun::PolygonByPoints> &building, const std::vector<double> &building_height,
                   const std::vector<sun::PolygonByPoints> &neighb_building,
                   const std::vector<double> &neighb_builds_heights) {

        if (tg_altitude <= 0 || !building.size()) {
            return 0;
        }
        const auto vect_segm = sun::normalize(sun::Vector2d(building.at(omit_build_ind).points_[segm_numb],
                                                 building.at(omit_build_ind).points_[(segm_numb + 1) % building.at(
                                                         omit_build_ind).points_.size()]));
        const auto ort = sun::orthog_vect_out(vect_segm);
        if (sun::scalar_product(ort, dir_to_sun) <= 0) {
            return 0;
        }
        const sun::Segment segm(point, point + dir_to_sun * sun::kRadisuToCheckNeigBuild); // normed dir_to_sun

        int check_self_intersection = ray_is_passing_through_build(omit_build_ind, segm_numb, point, point_height,
                                                                   tg_altitude, dir_to_sun, building, building_height,
                                                                   segm,
                                                                   false);

        if (check_self_intersection != 1) {
            return check_self_intersection;
        }
        int check_neighb_intersection = ray_is_passing_through_build(omit_build_ind, segm_numb, point, point_height,
                                                                     tg_altitude, dir_to_sun, neighb_building,
                                                                     neighb_builds_heights, segm, true);

        return check_neighb_intersection;
    }

    void calculate_daily_insolation(const sun::Input &input, const int day_ind,
                                         std::vector<std::vector<double>> &insol_on_windows_on_day,
                                         double &count_sunny_hours) {
        for (int hour = input.sun_path_diag().first_hour_day_ind()[day_ind];
             hour < input.sun_path_diag().last_hour_day_ind()[day_ind] + 1; ++hour) {
            const auto sun_coord = input.sun_path_diag().sp_diag_local()[day_ind][hour];
            const double tg_altitude =
                    sun_coord.altitude_sin_ / std::sqrt(1 - sun_coord.altitude_sin_ * sun_coord.altitude_sin_);
            const auto vect_to_sun = sun::Vector2d(sun_coord.x_, sun_coord.y_);
            if (sun_coord.altitude_sin_ < 0) {
                continue;
            }
            count_sunny_hours++;
            for (int build_ind = 0; build_ind < input.params_build().building_size(); ++build_ind) {
                const auto build_number = build_ind;
                int wind_id = -1;
                for (auto &point: input.calcul_params().windows_xy()[build_number].points_) {
                    wind_id++;
                    int h_ind = -1;
                    for (auto &h: input.otional_params().windows_z()[build_number]) {
                        h_ind++;
                        if (input.closed_windows().find(sun::Grid3D(build_ind, wind_id, h_ind)) !=
                            input.closed_windows().end()) {
                            continue;
                        }
                        auto res = static_cast<double>(ray_is_passing(build_number,
                                                                      input.calcul_params().walls_id()[build_number][wind_id],
                                                                      point, h,
                                                                      tg_altitude, vect_to_sun,
                                                                      input.params_build().get_buildings(),
                                                                      input.params_build().get_buildings_heights(),
                                                                      input.params_neighb_build().get_buildings(),
                                                                      input.params_neighb_build().get_buildings_heights()));
                        if (res == -1) {
                            res = 0;
                        }
                        insol_on_windows_on_day[build_ind][wind_id] += res;
                    }
                }
            }
        }
    }

} // namespace

double sun::calculate_insolation(const sun::Input &input) {
//  build_numbers - indexes bulduing from input.buildings;
    const int count_day = input.params_astro().days_ind_.size();
    std::vector<std::vector<double>> res_aver_on_wall, res_aver_on_window; // average by day
    for (int ind = 0; ind < input.params_build().building_size(); ++ind) {
        res_aver_on_wall.push_back(std::vector<double>(input.params_build().get_buildings().at(ind).points_.size()));
        res_aver_on_window.push_back(std::vector<double>(input.calcul_params().windows_xy()[ind].points_.size()));
    }
    std::vector<std::vector<double>> insol_on_windows = res_aver_on_window;
    std::vector<double> insol_on_building(input.params_build().building_size());

    for (int day_ind = 0; day_ind < input.params_astro().days_ind().size(); ++day_ind) {
        auto insol_on_windows_on_day = res_aver_on_window;
        double count_sunny_hours = 0;
        calculate_daily_insolation(input, day_ind, insol_on_windows_on_day, count_sunny_hours);

        for (int j = 0; j < insol_on_windows.size(); ++j) {
            for (int t = 0; t < insol_on_windows[j].size(); ++t) {
                if (count_sunny_hours) {
                    insol_on_windows[j][t] += insol_on_windows_on_day[j][t] / count_sunny_hours;
                }
            }
        }
    }
    double insolation = 0;
    int total_windows_count = 0;
    for (int build_ind = 0; build_ind < insol_on_windows.size(); ++build_ind) {
        auto build_number = build_ind;
        for (int t = 0; t < insol_on_windows[build_ind].size(); ++t) {
            insol_on_windows[build_ind][t] /= count_day;
            insol_on_building[build_ind] += insol_on_windows[build_ind][t];
            insolation += insol_on_windows[build_ind][t];
            auto wall_ind = input.calcul_params().walls_id()[build_number][t];
        }
        total_windows_count += input.calcul_params().count_wind_in_build()[build_number];
        insol_on_building[build_ind] /= input.calcul_params().windows_xy()[build_number].points_.size();
    }
    insolation /= total_windows_count;

    return insolation;
}





