//
// Created by ryhor on 05/07/19.
//

#include "input.h"

namespace {
    int calculate_wind_count(const sun::Segment &segm, const double step_, double &step_local) {
        const double dist = distance(segm.p1_, segm.p2_);
        int wind_count = static_cast<int>((dist - step_) / step_) + 1;
        const auto vect = normalize(sun::Vector2d(segm));
        step_local = step_;
        if (wind_count <= 0) {
            step_local = 0;
            wind_count = 1;
        }
        return wind_count;
    }

}
bool sun::Input::process_closed_windows(const int sect_ind, const int build_neigb_ind, const int point_neighb_ind,
        const sun::Point2d &point, const int k, int &wind_count_adj) {
    int wind_neihb_count_adj = 0, wall_neighb_ind = 0;
    wall_neighb_ind = calcul_params_.walls_id()[build_neigb_ind][point_neighb_ind];
    if (params_build_.get_buildings_heights().at(build_neigb_ind) >=
        otional_params_.windows_z()[sect_ind].back()) {
        for (int h_ind = 0; h_ind < otional_params_.windows_z()[build_neigb_ind].size(); ++h_ind) {
            if (params_build_.get_buildings_heights().at(sect_ind) >=
                otional_params_.windows_z()[build_neigb_ind][h_ind]) {
                closed_windows_.insert(Grid3D(build_neigb_ind, point_neighb_ind, h_ind));
                wind_neihb_count_adj++;
            }
        }
        wind_count_adj += otional_params_.windows_z()[sect_ind].size();
        calcul_params_.count_wind_on_wall()[build_neigb_ind][wall_neighb_ind] -= wind_neihb_count_adj;
        calcul_params_.count_wind_in_build()[build_neigb_ind] -= wind_neihb_count_adj;
        return true;
    } else {
        calcul_params_.windows_xy().back().points_.push_back(point);
        calcul_params_.walls_id().back().push_back(k);

        for (int h_ind = 0; h_ind < otional_params_.windows_z()[sect_ind].size(); ++h_ind) {
            if (params_build_.get_buildings_heights().at(build_neigb_ind) >=
                otional_params_.windows_z()[sect_ind][h_ind]) {
                closed_windows_.insert(
                        Grid3D(sect_ind,
                               calcul_params_.windows_xy().back().points_.size() - 1, h_ind));
                wind_count_adj++;
            }
        }
        for (int h_ind = 0; h_ind < otional_params_.windows_z()[build_neigb_ind].size(); ++h_ind) {
            closed_windows_.insert(Grid3D(build_neigb_ind, point_neighb_ind, h_ind));
            wind_neihb_count_adj++;
        }
        calcul_params_.count_wind_on_wall()[build_neigb_ind][wall_neighb_ind] -= wind_neihb_count_adj;
        calcul_params_.count_wind_in_build()[build_neigb_ind] -= wind_neihb_count_adj;
    }
    return false;
}

void sun::Input::calculate_windows_for_section(const sun::PolygonByPoints &section, const int sect_ind) {
    calcul_params_.windows_xy().push_back({});
    calcul_params_.walls_id().push_back({});
    inds_.push_back(sect_ind);
    std::vector<int> tmp_vect(section.points_.size());
    for (int k = 0; k < section.points_.size(); ++k) {
        if (otional_params_.closed_segments().size() && otional_params_.closed_segments()[sect_ind][k]) {
            continue;
        }
        double step_local;
        const Segment segm(section.points_[k], section.points_[(k + 1) % section.points_.size()]);
        auto wind_count = calculate_wind_count(segm, step_, step_local);
        calcul_params_.count_wind_in_build()[sect_ind] += wind_count;
        const double shift = (distance(segm) - (wind_count - 1) * step_) / 2;
        int wind_count_adj = 0;
        for (int i = 0; i < wind_count; ++i) {
            const auto point = segm.p1_ + normalize(Vector2d(segm)) * (shift + step_local * i);
            int build_neigb_ind, point_neighb_ind;
            const auto wind_closed = check_closed_windows(sect_ind, point, segm, build_neigb_ind,
                                                          point_neighb_ind);
            if (wind_closed &&
            process_closed_windows(sect_ind, build_neigb_ind, point_neighb_ind, point, k, wind_count_adj)) {
                continue;
            }
            calcul_params_.windows_xy().back().points_.push_back(point);
            calcul_params_.walls_id().back().push_back(k);
        }
        tmp_vect[k] = wind_count * otional_params_.windows_z()[sect_ind].size() - wind_count_adj;
        calcul_params_.count_wind_in_build()[sect_ind] -= wind_count_adj;
    }
    calcul_params_.count_wind_on_wall().push_back(tmp_vect);
}

void sun::Input::calculate_windows() {
    // calculate windows_xy_ points
    calcul_params_.windows_xy_.clear();
    int sect_ind = -1;
    calcul_params_.count_wind_in_build().assign(params_build_.building_size(), 0);
    for (auto &section: params_build_.get_buildings()) {
        sect_ind++;
        calculate_windows_for_section(section, sect_ind);
    }

};

void sun::Input::init_sun_path_diagram(const std::vector<int> &year_days, const double latitude_) {
// calculate sun_path diagram
    sun_path_diag_.latitude() = latitude_;
    sun_path_diag_.is_north() = latitude_ >= 0;
    std::vector<int> new_days_ind(year_days.size());
    for (int i = 0; i < new_days_ind.size(); ++i) {
        new_days_ind[i] = i;
    }
    sun_path_diag_.days_ind() = new_days_ind;

    sun_path_diag_.init(year_days, latitude_);

    sun_path_diag_.calculate_sun_coord();

}

bool
sun::Input::check_closed_windows(const int &build_ind, const Point2d &point, const Segment &segm, int &build_neigb_ind,
                                 int &point_neighb_ind) {                    //
    bool wind_closed = false;
    for (build_neigb_ind = 0; build_neigb_ind < calcul_params_.windows_xy().size(); ++build_neigb_ind) {
        if (build_neigb_ind == build_ind) {
            continue;
        }
        for (point_neighb_ind = 0;
             point_neighb_ind < calcul_params_.windows_xy()[build_neigb_ind].points_.size(); ++point_neighb_ind) {
            const auto point_neigb = calcul_params_.windows_xy()[build_neigb_ind].points_[point_neighb_ind];
            const int point_neigb_ind_next = (point_neighb_ind + 1) %
                    calcul_params_.windows_xy()[build_neigb_ind].points_.size();
            const auto point_neigb_next = calcul_params_.windows_xy()[build_neigb_ind].points_[point_neigb_ind_next];
            if (distance(point_neigb, point) < kEpsForPoint2d) {
                wind_closed = true;
                break;
            } else {
                if (distance(point_neigb, point) < 2 * step_) {
                    if (point_on_segment(point, Segment(point_neigb, point_neigb_next), kEpsForPoint2d) &&
                        point_on_segment(point_neigb, segm, kEpsForPoint2d)) {
                        wind_closed = true;
                        break;
                    }
                }
            }
        }
        if (wind_closed) {
            break;
        }
    }
    return wind_closed;
}

void sun::Input::set_astronom_params(const double latitude, const std::vector<int> &days_ind) {
    params_astro_.latitude() = latitude;
    params_astro_.days_ind() = days_ind;
    init_sun_path_diagram(days_ind, latitude);
}

void sun::Input::set_buildings(const std::vector<PolygonByPoints> &buildings,
        const std::vector<double> &buildings_heights) {
    params_build_.set_buildings(buildings, buildings_heights);
    if (otional_params_.windows_z().empty()) {
        otional_params_.windows_z().assign(params_build_.building_size(), {3});
    }
    calculate_windows();
}

void sun::Input::set_neighb_buildings(const std::vector<PolygonByPoints> &neighb_buildings,
                                 const std::vector<double> &neighb_buildings_heights) {
    params_neighb_build_.set_buildings(neighb_buildings, neighb_buildings_heights);
}

void sun::Input::set_buildings_src(const std::vector<PolygonByPoints> &buildings,
                              const std::vector<double> &buildings_heights) {
    service_params_.buildings_src() = buildings;
    service_params_.buildings_heights_src() = buildings_heights;
    if (otional_params_.windows_z().empty()) {
        otional_params_.windows_z().assign(buildings.size(), {3});
    }
}

void sun::Input::set_neighb_buildings_src(std::vector<PolygonByPoints> &neighb_buildings,
                                     std::vector<double> &neighb_buildings_heights) {
    service_params_.neighb_buildings_src_ = neighb_buildings;
    service_params_.neighb_builds_heights_src_ = neighb_buildings_heights;
}