//
// Created by ryhor on 01/08/18.
//

#ifndef INSOLATION_CALCULATOR_GEOMETRY2D_H
#define INSOLATION_CALCULATOR_GEOMETRY2D_H

#include <algorithm>
#include <cmath>
#include <iostream>
#include <map>
#include <math.h>
#include <experimental/optional>
#include <vector>

constexpr double kEPS = 1E-9; // to compare doubles
constexpr double kEPSSoft = 1E-3; // for compare geom object equality
constexpr double kEpsForPoint2d = 0.1; // for compare Point2d
constexpr double kInfin = 1e9;

using std::experimental::optional;
using std::experimental::nullopt;

namespace dbl {

    inline bool less(double x, double y, double eps = kEPS) {
        return x < y - eps;
    }

    inline bool equal(double x, double y, double eps = kEPS) {
        return !less(x, y, eps) && !less(y, x, eps);
    }

    inline bool equal_less(double x, double y, double eps) {
        return less(x, y, eps) || equal(x, y, eps);
    }
}

namespace sun {


    struct Point2d {

        Point2d() = default;

        Point2d(double x, double y) : x_{x}, y_{y} {};

        double x_ = 0.0, y_ = 0.0;
    };

    struct Segment {

        Segment() = default;

        Segment(Point2d p1, Point2d p2) : p1_{p1}, p2_{p2} {};

        Point2d p1_, p2_;
    };

    struct Vector2d {

        Vector2d() : x_(0), y_(0) {};

        Vector2d(double x, double y) : x_(x), y_(y) {};

        Vector2d(const Point2d &p1, const Point2d &p2) {
            x_ = p2.x_ - p1.x_;
            y_ = p2.y_ - p1.y_;
        }

        explicit Vector2d(const Point2d &p1) {
            x_ = p1.x_;
            y_ = p1.y_;
        }

        explicit Vector2d(const Segment &seg1) {
            x_ = seg1.p2_.x_ - seg1.p1_.x_;
            y_ = seg1.p2_.y_ - seg1.p1_.y_;
        }

        double x_ = 0.0, y_ = 0.0;
    };


    inline bool operator == (const Point2d &p1, const Point2d &p2) {
        return dbl::equal(p1.x_, p2.x_, kEpsForPoint2d) && dbl::equal(p1.y_, p2.y_, kEpsForPoint2d);
    }

    inline bool equal(const Point2d &p1, const Point2d &p2, double eps) {
        return dbl::equal(p1.x_, p2.x_, eps) && dbl::equal(p1.y_, p2.y_, eps);
    }

    // vector space operations
    inline Vector2d operator * (const Vector2d &vect, double coef) {
        return Vector2d(vect.x_ * coef, vect.y_ * coef);
    }

    inline Point2d operator + (const Point2d &point, const Vector2d &vect) {
        return Point2d(point.x_ + vect.x_, point.y_ + vect.y_);
    }

    inline Vector2d operator + (const Vector2d &vect1, const Vector2d &vect2) {
        return Vector2d(vect1.x_ + vect2.x_, vect1.y_ + vect2.y_);
    }

    struct PolygonByPoints {

        std::vector<Point2d> points_;
    };

    bool segm_is_intersected(const Point2d &a, const Point2d &b, const Point2d &c, const Point2d &d);

    optional<Point2d> lines_intersection(const Point2d &p1, const Point2d &p2, const Point2d &p3, const Point2d &p4);

    optional<Point2d> segments_intersection(const Point2d &p1, const Point2d &p2, const Point2d &p3, const Point2d &p4);

    bool point_on_segment(const Point2d &point, const Segment &segm, double eps);

    inline double distance(const Point2d &p1, const Point2d &p2) {
        return std::hypot(p2.x_ - p1.x_, p2.y_ - p1.y_);
    }

    inline double distance(const Segment &segm) {
        return distance(segm.p1_, segm.p2_);
    }

    inline double scalar_product(const Vector2d &v1, const Vector2d &v2) {
        return v1.x_ * v2.x_ + v1.y_ * v2.y_;
    }

    inline double vector_length(const Vector2d &vec) {
        return distance(Point2d(vec.x_, vec.y_), Point2d());
    }

    inline Vector2d normalize(const Vector2d &vec) {
        return Vector2d(vec.x_ / vector_length(vec), vec.y_ / vector_length(vec));
    }

    // to left or to out from contour for clock wise direction
    inline Vector2d orthog_vect_out(const Vector2d &vec) {
        return Vector2d(-vec.y_, vec.x_);
    }

} // namespace sun

#endif //INSOLATION_CALCULATOR_GEOMETRY2D_H
