//
// Created by ryhor on 29/04/19.
//

#ifndef INSOLATION_CALCULATOR_CELESTIAL_COORDINATE_SYSTEM_H
#define INSOLATION_CALCULATOR_CELESTIAL_COORDINATE_SYSTEM_H

#include <fstream>
#include <math.h>
#include <vector>
#include "settings.h"

namespace sun {

    struct SunCoord {

        SunCoord() = default;

        SunCoord(double azimuth_cos, double altitude_sin, double x, double y) : azimuth_cos_{azimuth_cos},
                                                                                altitude_sin_{altitude_sin}, x_{x},
                                                                                y_{y} {}

        double azimuth_cos_ = 0;
        double altitude_sin_ = 0;
        double x_ = 0;
        double y_ = 0;
    };

    struct InputSunCoord {

        InputSunCoord(const int &year_day, const int &hour, const double &latitude, const bool &is_north = true) :
                year_day_{year_day}, hour_{hour}, latitude_{latitude}, is_north_{is_north} {};

        double latitude_ = 0.0;
        int year_day_ = -1;
        int hour_ = -1;
        bool is_north_ = true;
    };

    class VirtSunPathDiagram {

    public:
        VirtSunPathDiagram() = default;

        virtual SunCoord get_sun_coord(const InputSunCoord &input) const { return SunCoord(); };

        int hour_bins_count_ = 2;
        int year_day_count_ = 0;
    };

    class SunPathDiagramV2 : public VirtSunPathDiagram {

    public:
        SunPathDiagramV2() : VirtSunPathDiagram() {};

        void calcul_celestial_coordinate_table(const std::vector<int> &year_days, const double latitude,
                const int hour_bins_count = 2);

        void init(const std::vector<int> &year_days, const int &latitude);

        SunCoord get_sun_coord(const InputSunCoord &input) const;

        void calculate_sun_coord();

        double &latitude() {
            return latitude_;
        }

        const double latitude() const {
            return latitude_;
        }

        std::vector<int> &days_ind() {
            return days_ind_;
        }

        const std::vector<int> &days_ind() const {
            return days_ind_;
        }

        bool &is_north() {
            return is_north_;
        }

        const bool is_north() const {
            return is_north_;
        }

        const std::vector<int> &first_hour_day_ind() const {
            return first_hour_day_ind_;
        }

        std::vector<int> &first_hour_day_ind() {
            return first_hour_day_ind_;
        }

        const std::vector<int> &last_hour_day_ind() const {
            return first_hour_day_ind_;
        }

        std::vector<int> &last_hour_day_ind() {
            return first_hour_day_ind_;
        }

        const std::vector<std::vector<SunCoord>> &sp_diag_local() const {
            return sp_diag_local_;
        }

    private:
        void calculate_day_angle_solar_declination_ET(const std::vector<int> &year_days,
        std::vector<int> &J, std::vector<double> &tau, std::vector<double> &delta, std::vector<double> &ET);

        void calculate_true_solar_time(const std::vector<double> &ET, std::vector<std::vector<double>> &ksi);

        void calculate_sin_altitude_cos_azimuth(const std::vector<double> &delta,
        const std::vector<std::vector<double>> &ksi);

    private:
        std::vector<std::vector<SunCoord>> sp_diag_local_;
        std::vector<std::vector<double>> solar_sin_altitude_;
        std::vector<std::vector<double>> solar_cos_azimuth_;
        std::vector<std::vector<char>> ksi_in_0_pi_;
        std::vector<int> days_ind_;
        std::vector<int> first_hour_day_ind_;
        std::vector<int> last_hour_day_ind_;
        double latitude_ = 0.0;
        // local hour index
        int first_hour_ind_ = -1;
        int last_hour_ind_ = -1;
        bool is_north_ = false;
    };

} // namespace sun

#endif //INSOLATION_CALCULATOR_CELESTIAL_COORDINATE_SYSTEM_H
