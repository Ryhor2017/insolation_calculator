//
// Created by ryhor on 7/4/18.
//

#ifndef INSOLATION_CALCULATOR_SOLVER_H
#define INSOLATION_CALCULATOR_SOLVER_H


#include "geometry2d.h"
#include "input.h"
#include "settings.h"

namespace sun {

    struct SolutionParams {

        SolutionParams() = default;

        SolutionParams(double angle) : angle_{angle} {};

        Point2d &origin() {
            return origin_;
        }

        const Point2d &origin() const {
            return origin_;
        }

        double &angle() {
            return angle_;
        }

        const double angle() const {
            return angle_;
        }

        Point2d origin_ = Point2d(0, 0);
        double angle_ = 0;
    };

    struct Solution {

        Solution() = default;

        Solution(const SolutionParams &params): params_{params} {};

        std::vector<PolygonByPoints> &buildings() {
            return buildings_;
        }

        const std::vector<PolygonByPoints> &buildings() const {
            return buildings_;
        }

        std::vector<PolygonByPoints> &windows_xy() {
            return buildings_;
        }

        const std::vector<PolygonByPoints> &windows_xy() const {
            return buildings_;
        }

        std::vector<std::vector<double>> &insol_by_wall() {
            return insol_by_wall_;
        }

        const std::vector<std::vector<double>> &insol_by_wall() const {
            return insol_by_wall_;
        }

        SolutionParams &params() {
            return params_;
        }

        const SolutionParams &params() const {
            return params_;
        }

        double &insolation() {
            return insolation_;
        }

        const double insolation() const {
            return insolation_;
        }

        std::vector<PolygonByPoints> buildings_;
        std::vector<PolygonByPoints> windows_xy_;
        std::vector<std::vector<double>> insol_by_wall_;
        SolutionParams params_;
        double insolation_;
    };

    double calculate_insolation(const Input &input);

} // namespace sun




#endif //INSOLATION_CALCULATOR_H
