#pragma once

#include <string>
#include <vector>

namespace sun {
    constexpr double kInf = 1e9;
    constexpr double kMinAltitudeToStartCalcul = 0.05; // use to ommit first and last hour
    constexpr double kEps = 0.01;
    constexpr double kMinAltitude = 0.30; // for calculate site_ envelop/ 0.15 = 10 градусов
    constexpr double kMinHeight = 0; //
    const std::vector<int> kPrecalculatedDays = {172, 356, 79, 266};
    constexpr int kWinterSolstice = 1; //356;
    constexpr int kSummerSolstice = 0; //172;
    constexpr int kRadisuToCheckNeigBuild = 300; //meter;
    constexpr double kMinDistBetwSectPoints = 0.1; //meter;
    const std::string kOutputDir = "../test_output/";
    constexpr double PI = 3.141592653589793238462643;
} // namespace sun
