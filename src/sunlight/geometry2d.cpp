//
// Created by ryhor on 01/08/18.
//

#include <fstream>
#include "geometry2d.h"


using std::pair;
using sun::Point2d;
using sun::Vector2d;
using sun::Segment;

namespace {

    inline double area (const Point2d &a, const Point2d &b, const Point2d &c) {
        return (b.x_ - a.x_) * (c.y_ - a.y_) - (b.y_ - a.y_) * (c.x_ - a.x_);
    }

    inline bool is_intersected(double a, double b, double c, double d) {
        if (dbl::less(b, a, kEpsForPoint2d))  std::swap (a, b);
        if (dbl::less(d, c, kEpsForPoint2d))  std::swap (c, d);
        return dbl::less(std::max(a, c), std::min(b, d), kEpsForPoint2d) || dbl::equal(std::max(a, c), std::min(b, d),
                                                                                       kEpsForPoint2d);
    }


    int sign(double x, double EPS) {
        if (x < - EPS) {
            return -1;
        }
        else {
            if (x > EPS) {
                return 1;
            }
        }
        return 0;
    }

} // namespace


bool sun::segm_is_intersected(const Point2d &a, const Point2d &b, const Point2d &c, const Point2d &d) {
	return is_intersected(a.x_, b.x_, c.x_, d.x_)
		&& is_intersected(a.y_, b.y_, c.y_, d.y_)
		&& (dbl::less(sign(area(a, b, c), kEpsForPoint2d) * sign(area(a, b, d), kEpsForPoint2d), 0, kEpsForPoint2d) ||
            dbl::equal(sign(area(a, b, c), kEpsForPoint2d) * sign(area(a, b, d), kEpsForPoint2d), 0, kEpsForPoint2d))
		&& (dbl::less(sign(area(c, d, a), kEpsForPoint2d) * sign(area(c, d, b), kEpsForPoint2d), 0, kEpsForPoint2d) ||
            dbl::equal(sign(area(c, d, a), kEpsForPoint2d) * sign(area(c, d, b), kEpsForPoint2d), 0, kEpsForPoint2d));
}


optional<Point2d> sun::lines_intersection(const Point2d &p1, const Point2d &p2, const Point2d &p3, const Point2d &p4) {
    const double x1 = p1.x_, y1 = p1.y_;
    const double x2 = p2.x_, y2 = p2.y_;
    const double x3 = p3.x_, y3 = p3.y_;
    const double x4 = p4.x_, y4 = p4.y_;
    const double den = ((x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4));
    optional<Point2d> res;
    if (!dbl::equal(den, 0, kEPSSoft)) {
        res = Point2d(((x1 * y2 - y1 * x2) * (x3 - x4) - (x1 - x2) * (x3 * y4 - y3 * x4)) / den,
            ((x1 * y2 - y1 * x2) * (y3 - y4) - (y1 - y2) * (x3 * y4 - y3 * x4)) / den);
    }
    return res;
}

optional<Point2d>
sun::segments_intersection(const Point2d &p1, const Point2d &p2, const Point2d &p3, const Point2d &p4) {
    optional<Point2d> res;
    if (!segm_is_intersected(p1, p2, p3, p4)) {
        return res;
    }
    return lines_intersection(p1, p2, p3, p4);
}

bool sun::point_on_segment(const Point2d &point, const Segment &segm, double eps) {
    if (point == segm.p1_ || point == segm.p2_ ) {
        return true;
    }
    const Vector2d vect = Vector2d(segm);
    const double det = vect.x_ * vect.x_ + vect.y_ * vect.y_;
    const double t = (1 / det) * (vect.x_ * (point.x_ - segm.p1_.x_)  + vect.y_ * (point.y_ - segm.p1_.y_));
    if (dbl::less(t, 0, eps) || dbl::less(1, t, eps)) {
        return false;
    } else {
        if (dbl::less(distance(point, segm.p1_ + vect * t), eps)) {
            return true;
        } else {
            return false;
        }
    }
}
