//
// Created by ryhor on 02/11/18.
//

#ifndef INSOLATION_CALCULATOR_INPUT_H
#define INSOLATION_CALCULATOR_INPUT_H

#include <set>
#include <tuple>
#include "celestial_coordinate_system.h"
#include "geometry2d.h"
#include "parameters.h"

namespace sun {

    struct Grid3D {

        Grid3D(int x, int y, int z): x_{x}, y_{y}, z_{z} {};

        int &x() {
            return x_;
        }

        const int x() const {
            return x_;
        }

        int &y() {
            return y_;
        }

        const int y() const {
            return y_;
        }

        int &z() {
            return z_;
        }

        const int z() const {
            return z_;
        }

        bool operator< (const Grid3D &cr) const {
            return std::tie(x_, y_, z_ )< std::tie(cr.x_, cr.y_, cr.z_);
        }

        int x_ = 0, y_ = 0, z_ = 0;
    };


    class Input {
    public:
        Input() = default;

        void set_astronom_params(const double latitude, const std::vector<int> &days_ind = kPrecalculatedDays);

        void set_buildings(const std::vector<PolygonByPoints> &buildings, const std::vector<double> &buildings_heights);

        void set_neighb_buildings(const std::vector<PolygonByPoints> &neighb_buildings,
                const std::vector<double> &neighb_buildings_heights);

        void set_buildings_src(const std::vector<PolygonByPoints> &buildings, const std::vector<double> &buildings_heights);

        void set_neighb_buildings_src(std::vector<PolygonByPoints> &neighb_buildings, std::vector<double>
                &neighb_buildings_heights);

        const ParamsAstro &params_astro() const {
            return params_astro_;
        }

        const ParamsBuildings &params_build() const {
            return params_build_;
        }

        const ParamsBuildings &params_neighb_build() const {
            return params_neighb_build_;
        }

        const OptionalParams &otional_params() const {
            return otional_params_;
        }

        const CalculatedParams &calcul_params() const {
            return calcul_params_;
        }

        const SunPathDiagramV2 &sun_path_diag() const {
            return sun_path_diag_;
        }

        const std::set<Grid3D> &closed_windows() const {
            return closed_windows_;
        }

        const ServiceParams &service_params() const {
            return service_params_;
        }

        PolygonByPoints &site() {
            return site_;
        }

        const PolygonByPoints &site() const {
            return site_;
        }

    private:
        void calculate_windows();

        void init_sun_path_diagram(const std::vector<int> &year_days, const double latitude_);

        bool check_closed_windows(const int &build_ind, const Point2d &point, const Segment &segm, int &build_neigb_ind,
                                  int &point_neighb_ind);

        bool process_closed_windows(const int sect_ind, const int build_neigb_ind, const int point_neighb_ind,
        const sun::Point2d &point, const int k, int &wind_count_adj);

        void calculate_windows_for_section(const sun::PolygonByPoints &section, const int sect_ind);

    private:

        SunPathDiagramV2 sun_path_diag_;
        CalculatedParams calcul_params_;
        ServiceParams service_params_;
        OptionalParams otional_params_;
        ParamsAstro params_astro_;
        ParamsBuildings params_build_;
        ParamsBuildings params_neighb_build_;
        PolygonByPoints site_;
        std::set<Grid3D> closed_windows_;
        std::vector<int> inds_;
        double step_ = 12; //distance between windows_xy_, meter
        bool is_north_ = true;
    };

} // namespace sun

#endif //INSOLATION_CALCULATOR_INPUT_H
