//
// Created by ryhor on 11/07/19.
//
#include "celestial_coordinate_system.h"

void sun::SunPathDiagramV2::calculate_day_angle_solar_declination_ET(const std::vector<int> &year_days,
        std::vector<int> &J, std::vector<double> &tau, std::vector<double> &delta, std::vector<double> &ET) {
    // 1. day angle
    for (int i = 0; i < year_day_count_; ++i) {
        J[i] = year_days[i] + 1;
        tau[i] = 2 * PI * (J[i] - 1) / 365;
        delta[i] = 0.006918 - 0.399912 * cos(tau[i]) + 0.070257 * sin(tau[i]) - 0.006758 * cos(2 * tau[i]) +
                   0.000907 * sin(2 * tau[i]) - 0.002697 * cos(3 * tau[i]) + 0.001480 * sin(3 * tau[i]);
        ET[i] = 0.170 * sin(4 * PI * (J[i] - 80) / 373) - 0.129 * sin(2 * PI * (J[i] - 8) / 355);
    }
}

void sun::SunPathDiagramV2::calculate_true_solar_time(const std::vector<double> &ET,
        std::vector<std::vector<double>> &ksi) {
    std::vector<std::vector<double>> TST(hour_bins_count_ * 24,
                                        std::vector<double>(year_day_count_)); // equation of time
    ksi_in_0_pi_.assign(hour_bins_count_ * 24, std::vector<char>(year_day_count_));

    ksi = TST;

    for (int time_interval = 0; time_interval < hour_bins_count_ * 24; ++time_interval) {
        for (int day = 0; day < year_day_count_; ++day) {
            double LT = static_cast<double>(time_interval) / hour_bins_count_;
            TST[time_interval][day] = LT + ET[day];
            ksi[time_interval][day] = (TST[time_interval][day] - 12) * 15 * PI / 180;
            if (ksi[time_interval][day] < 0) {
                ksi_in_0_pi_[time_interval][day] = true;
            }
        }
    }
}
void sun::SunPathDiagramV2::calculate_sin_altitude_cos_azimuth(const std::vector<double> &delta,
        const std::vector<std::vector<double>> &ksi) {
    solar_sin_altitude_.assign(hour_bins_count_ * 24, std::vector<double>(year_day_count_));
    solar_cos_azimuth_.assign(hour_bins_count_ * 24, std::vector<double>(year_day_count_));
    first_hour_ind_ = -1, last_hour_ind_ = -1;
    first_hour_day_ind_.assign(year_day_count_, -1);
    last_hour_day_ind_.assign(year_day_count_, -1);
    for (int time_interval = 0; time_interval < hour_bins_count_ * 24; ++time_interval) {
        bool one_day_alt_posit = false;
        for (int day = 0; day < year_day_count_; ++day) {
            // ToDo latitude must be in radians
            solar_sin_altitude_[time_interval][day] =
                    sin(latitude() * PI / 180) * sin(delta[day]) + cos(latitude() * PI / 180) * cos(delta[day]) *
                                                                 cos(ksi[time_interval][day]);

            const auto sin_a = solar_sin_altitude_[time_interval][day];
            const auto cos_a = sqrt(1 - sin_a * sin_a);
            double res_n = kInf;
            if (cos_a > kEps) {
                // if the sun isn't at its zenith, then azimuth is defined
                res_n = (-sin(latitude() * PI / 180) * sin_a + sin(delta[day])) /
                        (cos(latitude() * PI / 180) * cos_a);
            }
            solar_cos_azimuth_[time_interval][day] = res_n;

            if (solar_sin_altitude_[time_interval][day] > 0) {
                one_day_alt_posit = true;
                if (first_hour_day_ind_[day] == -1) {
                    first_hour_day_ind_[day] = time_interval;
                }
                last_hour_day_ind_[day] = time_interval;
            }
        }
        if (one_day_alt_posit) {
            if (first_hour_ind_ == -1) {
                first_hour_ind_ = time_interval;
            }
            last_hour_ind_ = time_interval;
        }
    }
}

void sun::SunPathDiagramV2::calcul_celestial_coordinate_table(const std::vector<int> &year_days, const double latitude,
        const int hour_bins_count) {
    year_day_count_ = year_days.size();
    latitude_ = latitude;
    hour_bins_count_ = hour_bins_count;

    // part 1
    std::vector<int> J(year_day_count_);
    std::vector<double> tau(year_day_count_), delta(year_day_count_), ET(year_day_count_); // day angle, solar declination, equation of time
    calculate_day_angle_solar_declination_ET(year_days, J, tau, delta, ET);

    // part 2  true solar time and 6. Hour angle
    std::vector<std::vector<double>> ksi;
    calculate_true_solar_time(ET, ksi);

    // part 3
    calculate_sin_altitude_cos_azimuth(delta, ksi);

    // part 4 prepare hour's index
    for (int j = 0; j < year_day_count_; ++j) {
        if (first_hour_day_ind_[j] == -1) { // sun doesn't rise this day
            first_hour_day_ind_[j] = 0;
            last_hour_day_ind_[j] = 0;
        } else {
            first_hour_day_ind_[j] -= first_hour_ind_;
            last_hour_day_ind_[j] -= first_hour_ind_;
        }
    }

    ksi_in_0_pi_.assign(ksi_in_0_pi_.begin() + first_hour_ind_, ksi_in_0_pi_.begin() + last_hour_ind_ + 1);
    solar_sin_altitude_.assign(solar_sin_altitude_.begin() + first_hour_ind_, solar_sin_altitude_.begin() + last_hour_ind_ + 1);
    solar_cos_azimuth_.assign(solar_cos_azimuth_.begin() + first_hour_ind_, solar_cos_azimuth_.begin() + last_hour_ind_ + 1);

}

void sun::SunPathDiagramV2::init(const std::vector<int> &year_days, const int &latitude) {
    calcul_celestial_coordinate_table(year_days, latitude);
}

sun::SunCoord sun::SunPathDiagramV2::get_sun_coord(const InputSunCoord &input) const{
    const double y = solar_cos_azimuth_.at(input.hour_).at(input.year_day_);
    double x = sqrt(1 - y * y);
    if (!ksi_in_0_pi_[input.hour_][input.year_day_]) {
        x *= -1;
    }

    return SunCoord(solar_cos_azimuth_.at(input.hour_).at(input.year_day_),
            solar_sin_altitude_.at(input.hour_).at(input.year_day_), x, y);
}

void sun::SunPathDiagramV2::calculate_sun_coord() {
    SunCoord sun_coord_base(-1, -1, -1, -1);
    sp_diag_local_.assign(days_ind_.size(),
                         std::vector<SunCoord>(last_hour_ind_ - first_hour_ind_ + 1,
                                               sun_coord_base));

    int day_ind = -1;
    for (int day: days_ind_) {
        day_ind++;
        for (int hour = first_hour_day_ind_[day_ind];
             hour < last_hour_day_ind_[day_ind] + 1; ++hour) {
            sp_diag_local_[day_ind][hour] = get_sun_coord(InputSunCoord(day, hour, latitude_));
        }
    }
}