//
// Created by ryhor on 17/07/19.
//
#include <cmath>

#include "settings.h"

#include "make_beam_range.h"

std::vector<p3> make_beam_range(const double latitude, const int day, const int hour_bins_count) {
    constexpr double PI = 3.141592653589793238462643;
    constexpr double kInf = 1e9;
    constexpr double kEps = 0.01;

    const int J = day + 1;
    const double tau = 2 * PI * (J - 1) / 365;
    const double delta = 0.006918 - 0.399912 * cos(tau) + 0.070257 * sin(tau) - 0.006758 * cos(2 * tau) +
                   0.000907 * sin(2 * tau) - 0.002697 * cos(3 * tau) + 0.001480 * sin(3 * tau);
    const double ET = 0.170 * sin(4 * PI * (J - 80) / 373) - 0.129 * sin(2 * PI * (J - 8) / 355);

    std::vector<double> TST(hour_bins_count * 24); // equation of time
    std::vector<int> ksi_in_0_pi_(hour_bins_count * 24);
    auto ksi = TST;

    for (int time_interval = 0; time_interval < hour_bins_count * 24; ++time_interval) {
        double LT = static_cast<double>(time_interval) / hour_bins_count;
        TST[time_interval] = LT + ET;
        ksi[time_interval] = (TST[time_interval] - 12) * 15 * PI / 180;
        if (ksi[time_interval] < 0) {
            ksi_in_0_pi_[time_interval] = true;
        }
    }

    std::vector<double> solar_sin_altitude(hour_bins_count * 24);
    std::vector<double> solar_cos_azimuth(hour_bins_count * 24);
    int first_hour_ind = -1, last_hour_ind = -1;
    for (int time_interval = 0; time_interval < hour_bins_count * 24; ++time_interval) {
        // ToDo latitude must be in degree
        solar_sin_altitude[time_interval] =
                sin(latitude * PI / 180) * sin(delta) + cos(latitude * PI / 180) * cos(delta) *
                                                        cos(ksi[time_interval]);

        const auto sin_a = solar_sin_altitude[time_interval];
        const auto cos_a = sqrt(1 - sin_a * sin_a);
        double res_n = kInf;
        if (cos_a > kEps) {
            // if the sun isn't at its zenith, then azimuth is defined
            res_n = (-sin(latitude * PI / 180) * sin_a + sin(delta)) /
                    (cos(latitude * PI / 180) * cos_a);
        }
        solar_cos_azimuth[time_interval] = res_n;

        if (solar_sin_altitude[time_interval] > 0) {
            if (first_hour_ind == -1) {
                first_hour_ind = time_interval;
            }
            last_hour_ind = time_interval;
        }
    }

    ksi_in_0_pi_.assign(ksi_in_0_pi_.begin() + first_hour_ind, ksi_in_0_pi_.begin() + last_hour_ind + 1);
    solar_sin_altitude.assign(solar_sin_altitude.begin() + first_hour_ind, solar_sin_altitude.begin() + last_hour_ind + 1);
    solar_cos_azimuth.assign(solar_cos_azimuth.begin() + first_hour_ind, solar_cos_azimuth.begin() + last_hour_ind + 1);

    std::vector<p3> res(last_hour_ind - first_hour_ind + 1);
    for (int time_interval = 0; time_interval < last_hour_ind - first_hour_ind + 1; ++time_interval) {
        double y = solar_cos_azimuth.at(time_interval);
        double x;
        if (y > kInf / 2) {
            y = 0;
            x = 0;
        } else {
            x = sqrt(1 - y * y);
        }
        if (!ksi_in_0_pi_[time_interval]) {
            x *= -1;
        }
        double z = solar_sin_altitude[time_interval];
        auto vect_len = std::sqrt(std::pow(x, 2) + std::pow(y, 2) + std::pow(z, 2));
        res[time_interval] = {x / vect_len, y / vect_len, z / vect_len};
    }
    return res;
}