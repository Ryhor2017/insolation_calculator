//
// Created by ryhor on 11/07/19.
//

#ifndef INSOLATION_CALCULATOR_PARAMETERS_H
#define INSOLATION_CALCULATOR_PARAMETERS_H

#include <vector>

struct ParamsAstro {

    double &latitude() {
        return latitude_;
    }

    const double latitude() const {
        return latitude_;
    }

    std::vector<int> &days_ind() {
        return days_ind_;
    }

    const std::vector<int> &days_ind() const {
        return days_ind_;
    }

    // astronomical
    double latitude_ = 0;
	std::vector<int> days_ind_;
};

struct ParamsBuildings {

    void set_buildings(const std::vector<sun::PolygonByPoints> &buildings, const std::vector<double> &buildings_heights) {
        buildings_ = &buildings;
        buildings_heights_ = &buildings_heights;
        building_size_ = buildings.size();
    }

    const std::vector<sun::PolygonByPoints>& get_buildings() const {
        return *buildings_;
    }

    const std::vector<double>& get_buildings_heights() const {
        return *buildings_heights_;
    }

    const int building_size() const {
        return building_size_;
    }

    // our buildings
    int building_size_ = 0;
    const std::vector<double> *buildings_heights_ = nullptr;
    const std::vector<sun::PolygonByPoints> *buildings_ = nullptr;
};

struct OptionalParams {

    std::vector<std::vector<double>> &windows_z() {
        return windows_z_;
    }

    const std::vector<std::vector<double>> &windows_z() const {
        return windows_z_;
    }

    std::vector<std::vector<int>> &closed_segments() {
        return closed_segments_;
    }

    const std::vector<std::vector<int>> &closed_segments() const {
        return closed_segments_;
    }

    // information about closed walls
    std::vector<std::vector<int>> closed_segments_;
    std::vector<std::vector<double>> windows_z_;
};

struct CalculatedParams {

    std::vector<sun::PolygonByPoints> &windows_xy() {
        return windows_xy_;
    }

    const std::vector<sun::PolygonByPoints> &windows_xy() const {
        return windows_xy_;
    }

    std::vector<int> &count_wind_in_build() {
        return count_wind_in_build_;
    }

    const std::vector<int> &count_wind_in_build() const {
        return count_wind_in_build_;
    }

    std::vector<std::vector<int>> &count_wind_on_wall() {
        return count_wind_on_wall_;
    }

    const std::vector<std::vector<int>> &count_wind_on_wall() const {
        return count_wind_on_wall_;
    }

    std::vector<std::vector<int>> &walls_id() {
        return walls_id_;
    }

    const std::vector<std::vector<int>> &walls_id() const {
        return walls_id_;
    }

    std::vector<int> count_wind_in_build_;
    std::vector<std::vector<int>> walls_id_;
    std::vector<std::vector<int>> count_wind_on_wall_;
    std::vector<sun::PolygonByPoints> windows_xy_;
};

struct ServiceParams {

    std::vector<sun::PolygonByPoints> &buildings_src() {
        return buildings_src_;
    }

    const std::vector<sun::PolygonByPoints> &buildings_src() const {
        return buildings_src_;
    }
    std::vector<sun::PolygonByPoints> &neighb_buildings_src() {
        return buildings_src_;
    }

    const std::vector<sun::PolygonByPoints> &neighb_buildings_src() const {
        return neighb_buildings_src_;
    }

    std::vector<double> &buildings_heights_src() {
        return buildings_heights_src_;
    }

    const std::vector<double> &buildings_heights_src() const {
        return buildings_heights_src_;
    }

    std::vector<double> &neighb_builds_heights_src() {
        return neighb_builds_heights_src_;
    }

    const std::vector<double> &neighb_builds_heights_src() const {
        return buildings_heights_src_;
    }

    std::vector<double> buildings_heights_src_;
    std::vector<double> neighb_builds_heights_src_;
    std::vector<sun::PolygonByPoints> buildings_src_;
    std::vector<sun::PolygonByPoints> neighb_buildings_src_;
};

#endif //INSOLATION_CALCULATOR_PARAMETERS_H

