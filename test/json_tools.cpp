//
// Created by ryhor on 11/07/19.
//

#include "json_tools.h"
#include <chrono>

using json = nlohmann::json;

void CreateVisualJSON(const sun::Input &input, const std::string &file_name) {
    sun::Solution solution;
    solution.buildings_ = input.params_build().get_buildings();
    solution.insolation_ = -1;
    solution.insol_by_wall_ = {};
    solution.windows_xy_ = input.calcul_params().windows_xy();
    solution.params_.angle_ = 0;
    CreateVisualJSON(solution, input, file_name);
}

void CreateVisualJSON(const sun::Solution &solution, const sun::Input &input, const std::string &file_name) {
        // save result to json for visualization

    json root, sun_path, buildings_json, neigh_buildings_json, insolation_by_wall;

    for (auto building: solution.buildings_) {
        json building_json;
        for (auto point: building.points_) {
            json pn;
            pn["x"] = point.x_;
            pn["y"] = point.y_;
            building_json.push_back(pn);
        }
        buildings_json.push_back(building_json);
    }
    root["buildings"] = buildings_json;
    if (input.params_neighb_build().get_buildings().size()) {
        for (auto building: input.params_neighb_build().get_buildings()) {
            json building_json;
            for (auto point: building.points_) {
                json pn;
                pn["x"] = point.x_;
                pn["y"] = point.y_;
                building_json.push_back(pn);
            }
            neigh_buildings_json.push_back(building_json);
        }
        root["neig_buildings"] = neigh_buildings_json;
    }

    if (solution.insol_by_wall_.size()) {
        root["insolation_by_wall"] = solution.insol_by_wall_;
    }

    for (auto days: input.sun_path_diag().sp_diag_local()) {
        json days_js;
        for (auto sun_coord: days) {
            json coord;
            coord["x"] = sun_coord.x_;
            coord["y"] = sun_coord.y_;
            coord["alt_sin"] = sun_coord.altitude_sin_;
            days_js.push_back(coord);
        }
        sun_path.push_back(days_js);
    }

    std::vector<std::vector<double>> windows_json;
    int build_ind = -1;
    for (auto wind_of_build: solution.windows_xy_) {
        build_ind++;
        int wind_ind = -1;
        for (auto point: wind_of_build.points_) {
            wind_ind++;
            bool wind_closed = true;
            for (int h = 0; h < input.otional_params().windows_z()[build_ind].size(); ++h) {
                if (input.closed_windows().find(sun::Grid3D(build_ind, wind_ind, h)) == input.closed_windows().end()) {
                    wind_closed = false;
                    break;
                }
            }
            if (!wind_closed) {
                windows_json.push_back({point.x_, point.y_});
            }
        }
    }
    root["windows"] = windows_json;
    root["days"] = input.params_astro().days_ind();

    root["sun_path_diag"] = sun_path;
    root["map_wind_to_wall"] = input.calcul_params().walls_id();

    root["latitude"] = input.params_astro().latitude();
    if (input.params_neighb_build().get_buildings_heights().size()) {
        root["neib_build_height"] = input.params_neighb_build().get_buildings_heights();
    }
    root["build_height"] = input.params_build().get_buildings_heights();
    root["longitude"] = 0;
    root["time_zone"] = 0;
    root["angle"] = solution.params_.angle_;
    root["integral_insolation"] = solution.insolation_;

    std::ofstream file(file_name);

    file << root;
    file.close();
}

int ReadJSONInput(const std::string &file_name, sun::Input &input, bool read_binary) {
    try {


        std::ifstream in;
        in.open(file_name);
        json input_json;
        in >> input_json;
        // test simple json parsing
       // ju::parse_json(input_json, "closed_segments", input.otional_params().closed_segments_);

        // ju::parse_json(input_json, "win_heights", input.windows_z_);

        std::vector<int> days_ind;
        ju::parse_json(input_json, "days", days_ind);

        // ToDo
        auto start_solver = std::chrono::high_resolution_clock::now();
        input.set_astronom_params(input_json["latitude"].get<float>(), days_ind);
        auto finish_solver = std::chrono::high_resolution_clock::now();
        std::chrono::duration<double> init_elapsed = finish_solver - start_solver;
        std::cout << "set_astronom_params time: " << init_elapsed.count() << std::endl;

        std::vector<double> buildings_heights;
        ju::parse_json(input_json, "buildings_heights", buildings_heights);

        std::vector<double> neighb_builds_heights;
        ju::parse_json(input_json, "neighb_builds_heights", neighb_builds_heights);

        std::vector<double> neighb_bottom_windows_heights;
        ju::parse_json(input_json, "neighb_bottom_windows_heights", neighb_bottom_windows_heights);

        for (auto &point: input_json["site_"]) {
            input.site().points_.push_back(sun::Point2d(point.at(0).get<float>(), point.at(1).get<float>()));
        }

        std::vector<sun::PolygonByPoints> buildings;
        int ind = -1;
        for (auto &build: input_json["buildings"]) {
            buildings.push_back(sun::PolygonByPoints());
            ind++;
            for (auto &point: build) {
                buildings[ind].points_.push_back(sun::Point2d(point.at(0).get<float>(), point.at(1).get<float>()));
            }
        }
        input.set_buildings_src(buildings, buildings_heights);

        std::vector<sun::PolygonByPoints> neighb_buildings;
        ind = -1;
        for (auto &build: input_json["neighb_buildings"]) {
            neighb_buildings.push_back(sun::PolygonByPoints());
            ind++;
            for (auto &point: build) {
                neighb_buildings[ind].points_.push_back(
                        sun::Point2d(point.at(0).get<float>(), point.at(1).get<float>()));
            }
        }
        input.set_neighb_buildings_src(neighb_buildings, neighb_builds_heights);
    }
    catch (std::ifstream::failure &e) {
        std::cerr << "Exception open/reading file: " << e.what() << '\n';
        return 1;
    }
    return 0;
}
