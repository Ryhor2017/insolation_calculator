#include <chrono>
#include <iostream>
#include <string>
#include <type_traits>

#include "../src/sunlight/solver.h"
#include "../src/sunlight/geometry2d.h"
#include "json_tools.h"
#include "../src/sunlight/make_beam_range.h"

using json = nlohmann::json;


int main() {
    // create save sun_puth_diagram to json file
//    calcul_celestial_coordinate_table();

    sun::Input input;
    ReadJSONInput("../test_input/test_input_optimyzer_new_5sect.json", input, true);

//        auto start_solver = std::chrono::high_resolution_clock::now();
//    auto finish_solver = std::chrono::high_resolution_clock::now();
//    std::chrono::duration<double> init_elapsed = finish_solver - start_solver;
//    std::cout << "calculate_windows time: " << init_elapsed.count() << std::endl;

    // --------- create envelop
//    std::vector<double> site_envelop;
//
//    bool is_north_ = true;
//    PolygonByPoints site_grid;

//    CalculateSiteEnvelop(sun_path_diag_, input, site_grid, site_envelop);
//    Prepare3DVisualization(input, sun_path_diag_, site_grid, site_envelop);
    // --------- / create envelop
    std::vector<int> all_days(365);
    for (int i = 0; i < 365; ++i) {
        all_days[i] = i + 1;
    }

    for (double latitude: {5.0, 15.0, 30.0, 50.0, 60.0, 70.95}) {
        auto start_solver = std::chrono::high_resolution_clock::now();
        input.set_astronom_params(latitude, {172, 356, 79, 266});

    // ------------------- optimization task
        input.set_buildings(input.service_params().buildings_src(), input.service_params().buildings_heights_src());

        input.set_neighb_buildings(input.service_params().neighb_buildings_src(), input.service_params().neighb_builds_heights_src());
        auto res = calculate_insolation(input);
        auto finish_solver = std::chrono::high_resolution_clock::now();
        //Solver(input, output, true);
        // ------------------- / optimization task

        std::chrono::duration<double> init_elapsed = finish_solver - start_solver;
        std::cout << "init time: " << init_elapsed.count() << std::endl;
        std::cout << "res insol: " << res << std::endl;
        CreateVisualJSON(input, "output_test.json");
    }

    auto res = make_beam_range(70.95, 172, 3);
    for (auto xyz: res) {
        std::cout << sqrt(xyz[0] * xyz[0] + xyz[1] * xyz[1] + xyz[2] * xyz[2]) << std::endl;
        std::cout << xyz[0] << " " << xyz[1] << " " << xyz[2] << std::endl;
    }
    return 0;
}



