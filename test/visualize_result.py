import json
import numpy as np
import matplotlib
# matplotlib.use('Qt4Agg')
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import matplotlib as mpl
import os

files = ["output_test.json", "output_test_best.json", "output_test_worst.json", "output_test_allres.json"]

work_dir = "/home/ryhor/Algorithms/insolation_calculator/test/cmake-build-debug/"
res = []

for file_name in files:
    res_dict = {}
    try:
        file = open(work_dir + file_name, 'r')
        json_string = file.read()
        file.close()
        res_dict = json.loads(json_string)
    except Exception as e:
        pass
    res.append(res_dict)


json_dict_1, json_dict_2, json_dict_3, json_dict_4 = res

warm_day = np.argmin(np.abs(np.array(json_dict_1["days"]) - 180))
cold_day1 = np.argmin(np.abs(np.array(json_dict_1["days"]) - 0))
cold_day2 = np.argmin(np.abs(np.array(json_dict_1["days"]) - 360))

cold_day = cold_day1
if np.abs(np.array(json_dict_1["days"]) - 0)[cold_day1] > np.abs(np.array(json_dict_1["days"]) - 360)[cold_day2]:
    cold_day = cold_day2

day_inds = [cold_day];
if warm_day != cold_day:
    day_inds += [warm_day]

day_colors = ["blue", "red"]


show_windows = True
simple_mode = True
show_wind_insol = False

contour = list(map(lambda x: list(map(lambda y: list((y["x"], y["y"])), x)), json_dict_1["buildings"]))
contour_neig = list(map(lambda x: list(map(lambda y: list((y["x"], y["y"])), x)), json_dict_1["neig_buildings"]))

degree_text_size = 7


radius = 5
# visualize contour and columns
fig1 = plt.figure()
ax1 = fig1.add_subplot(121)

ax_best = fig1.add_subplot(243)
ax_worst = fig1.add_subplot(244)
ax_aver = fig1.add_subplot(247, polar=True)
ax_min = fig1.add_subplot(248, polar=True)

max_res = -1
ind = -1
for contour_i in contour:
    ind += 1
    fill = True
    #if ind == 0:
    #    fill = False
    ax1.add_patch(mpatches.Polygon(np.array(contour_i),  # (x,y)
                                       color='orange',
                                        fill=fill,
                                       linewidth = 1,
                                       alpha=0.6,
                                        linestyle = '-'
                                       ))

    loc_max = max(list(map(lambda x: np.sqrt(x[0]**2+x[1]**2), np.array(contour_i))))
    if loc_max > max_res:
        max_res = loc_max
    cent = np.mean(np.array(contour_i), axis=0)
    ax1.text(cent[0], cent[1], "H = " + str(round(json_dict_1["build_height"][ind])), size=6)

ind = -1
for contour_i in contour_neig:
    ind += 1
    fill = True
    #if ind == 0:
    #    fill = False
    ax1.add_patch(mpatches.Polygon(np.array(contour_i),  # (x,y)
                                       color='orange',
                                        fill=fill,
                                       linewidth = 1,
                                       alpha=0.6,
                                        linestyle = '-'
                                       ))

    loc_max = max(list(map(lambda x: np.sqrt(x[0]**2+x[1]**2), np.array(contour_i))))
    if loc_max > max_res:
        max_res = loc_max
    cent = np.mean(np.array(contour_i), axis=0)
    ax1.text(cent[0], cent[1], "H = " + str(round(json_dict_1["neib_build_height"][ind])), size=6)

# windows

def colorFader(c1,c2,mix=0): #fade (linear interpolate) from color c1 (at mix=0) to c2 (mix=1)
    c1=np.array(mpl.colors.to_rgb(c1))
    c2=np.array(mpl.colors.to_rgb(c2))
    return mpl.colors.to_hex((1-mix)*c1 + mix*c2)

c1='#1B1BB3' #cold
c2='#FF0000' #hot
win_ind = -1
wind_len = 1.5
wind_width = 0.1


if show_windows:
    if simple_mode:
        ind = -1
        for point in json_dict_1["windows"]:
            ind += 1
            ax1.add_patch(mpatches.Circle(np.array(point), 0.05,  # (x,y)
                                          color='blue',
                                          # closed=False,
                                          fill=True,
                                          linewidth=0.5,
                                          alpha=1,
                                          ))





radius = 3 * max_res
# axis
ax1.add_patch(mpatches.Polygon(np.array(((0, radius), (0, -radius))), closed=False,  # (x,y)
                                   color='gray',
                                    fill=False,
                                   linewidth = 1,
                                   alpha=0.6,
                                    linestyle = '-'
                                   ))

ax1.add_patch(mpatches.Polygon(np.array(((radius, 0), (-radius, 0))), closed=False,  # (x,y)
                                   color='gray',
                                    fill=False,
                                   linewidth = 1,
                                   alpha=0.6,
                                    linestyle = '-'
                                   ))

ax1.add_patch(mpatches.Polygon(np.array(((1/3.*radius*np.sin(np.pi/6), 1/3.*radius*np.cos(np.pi/6)), (radius*np.sin(np.pi/6), radius*np.cos(np.pi/6)))), closed=False,  # (x,y)
                                   color='gray',
                                    fill=False,
                                   linewidth = 1,
                                   alpha=0.6,
                                    linestyle = '--'
                                   ))
ax1.text(radius*np.sin(np.pi/6), radius*np.cos(np.pi/6), "30°", size=degree_text_size)

ax1.add_patch(mpatches.Polygon(np.array(((1/3.*radius*np.cos(np.pi/6), 1/3.*radius*np.sin(np.pi/6)), (radius*np.cos(np.pi/6), radius*np.sin(np.pi/6)))), closed=False,  # (x,y)
                                   color='gray',
                                    fill=False,
                                   linewidth = 1,
                                   alpha=0.6,
                                    linestyle = '--'
                                   ))
ax1.text(radius*np.cos(np.pi/6), radius*np.sin(np.pi/6), "60°", size=degree_text_size)

ax1.add_patch(mpatches.Polygon(np.array(((1/3.*radius*np.sin(np.pi/6), -1/3.*radius*np.cos(np.pi/6)), (radius*np.sin(np.pi/6), -radius*np.cos(np.pi/6)))), closed=False,  # (x,y)
                                   color='gray',
                                    fill=False,
                                   linewidth = 1,
                                   alpha=0.6,
                                    linestyle = '--'
                                   ))
ax1.text(radius*np.sin(np.pi/6), -radius*np.cos(np.pi/6), "150°", size=degree_text_size)

ax1.add_patch(mpatches.Polygon(np.array(((1/3.*radius*np.cos(np.pi/6), -1/3.*radius*np.sin(np.pi/6)), (radius*np.cos(np.pi/6), -radius*np.sin(np.pi/6)))), closed=False,  # (x,y)
                                   color='gray',
                                    fill=False,
                                   linewidth = 1,
                                   alpha=0.6,
                                    linestyle = '--'
                                   ))
ax1.text(radius*np.cos(np.pi/6), -radius*np.sin(np.pi/6), "120°", size=degree_text_size)

ax1.add_patch(mpatches.Polygon(np.array(((-1/3.*radius*np.sin(np.pi/6), -1/3.*radius*np.cos(np.pi/6)), (-radius*np.sin(np.pi/6), -radius*np.cos(np.pi/6)))), closed=False,  # (x,y)
                                   color='gray',
                                    fill=False,
                                   linewidth = 1,
                                   alpha=0.6,
                                    linestyle = '--'
                                   ))
ax1.text(-radius*np.sin(np.pi/6), -radius*np.cos(np.pi/6), "210°", size=degree_text_size)

ax1.add_patch(mpatches.Polygon(np.array(((-1/3.*radius*np.cos(np.pi/6), -1/3.*radius*np.sin(np.pi/6)), (-radius*np.cos(np.pi/6), -radius*np.sin(np.pi/6)))), closed=False,  # (x,y)
                                   color='gray',
                                    fill=False,
                                   linewidth = 1,
                                   alpha=0.6,
                                    linestyle = '--'
                                   ))
ax1.text(-radius*np.cos(np.pi/6), -radius*np.sin(np.pi/6), "240°", size=degree_text_size)

ax1.add_patch(mpatches.Polygon(np.array(((-1/3.*radius*np.sin(np.pi/6), 1/3.*radius*np.cos(np.pi/6)), (-radius*np.sin(np.pi/6), radius*np.cos(np.pi/6)))), closed=False,  # (x,y)
                                   color='gray',
                                    fill=False,
                                   linewidth = 1,
                                   alpha=0.6,
                                    linestyle = '--'
                                   ))
ax1.text(-radius*np.sin(np.pi/6), radius*np.cos(np.pi/6), "330°", size=degree_text_size)

ax1.add_patch(mpatches.Polygon(np.array(((-1/3.*radius*np.cos(np.pi/6), 1/3.*radius*np.sin(np.pi/6)), (-radius*np.cos(np.pi/6), radius*np.sin(np.pi/6)))), closed=False,  # (x,y)
                                   color='gray',
                                    fill=False,
                                   linewidth = 1,
                                   alpha=0.6,
                                    linestyle = '--'
                                   ))
ax1.text(-radius*np.cos(np.pi/6), radius*np.sin(np.pi/6), "300°", size=degree_text_size)

ax1.text(0, radius, "0°", size=degree_text_size)
ax1.text(0, -radius, "180°", size=degree_text_size)
ax1.text(radius, 0, "90°", size=degree_text_size)
ax1.text(-radius, 0, "270°", size=degree_text_size)

ax1.text(0, radius * 1.05, "N", size=10, color = "red")
ax1.text(0, -radius * 1.05, "S", size=10, color = "red")
ax1.text(radius * 1.05, 0, "E", size=10, color = "red")
ax1.text(-radius * 1.05, 0, "W", size=10, color = "red")

ax1.add_patch(mpatches.Circle((0, 0), radius, fill=False, color="gray"))
ax1.add_patch(mpatches.Circle((0, 0), radius * 2/3., fill=False, color="gray", linewidth = 0.5, alpha=0.3 ))
ax1.text(0, radius * 2/3., "30°", size=degree_text_size)
ax1.add_patch(mpatches.Circle((0, 0), radius * 1/3., fill=False, color="gray", linewidth = 0.5, alpha=0.3 ))
ax1.text(0, radius * 1/3., "60°", size=degree_text_size)
ax1.add_patch(mpatches.Circle((0, 0), radius, fill=False, color="gray"))
ax1.add_patch(mpatches.Circle((0, 0), radius * (2/3.+1/6.), fill=False, color="gray", linewidth = 0.5, alpha=0.3 ))
ax1.text(0, radius * (2/3.+1/6.), "15°", size=degree_text_size)
ax1.add_patch(mpatches.Circle((0, 0), radius * (1/3.+1./6), fill=False, color="gray", linewidth = 0.5, alpha=0.3 ))
ax1.text(0, radius * (1/3.+1./6), "45°", size=degree_text_size)

if not json_dict_1.get("insolation_by_wall") is None:
    shift_txt = 0.5
    build_ind = -1
    for contour_i in contour:
        build_ind += 1
        ind = -1
        for p1 in contour_i:
            ind += 1

            p1 = np.array(contour_i[ind])

            ind_next = ind + 1
            if ind_next > len(contour_i) - 1:
                ind_next = 0
            p2 = np.array((contour_i)[ind_next])
            vect = (p2 - p1) / np.sqrt(np.sum((p2 - p1) ** 2))
            ort_vect = np.array([vect[1], - vect[0]])

            ind_next = ind + 1
            if ind_next > len(contour_i) - 1:
                ind_next = 0
            p2 = (contour_i)[ind_next]

            ax1.text(0.5 * (p1[0] + p2[0]) + shift_txt * ort_vect[0], 0.5 * (p1[1] + p2[1]) + shift_txt * ort_vect[1], str(round(json_dict_1.get("insolation_by_wall")[build_ind][ind], 2)), size=6, )

small_coff = 0.03
col_ind = -1
if not json_dict_1.get("sun_path_diag") is None:
    for day_ind in day_inds:
        col_ind += 1
        ind = -1
        for pos in json_dict_1.get("sun_path_diag")[day_ind]:
            ind += 1
            if pos["alt_sin"] < 0:
                continue
            alpha = 1 - np.arcsin(pos["alt_sin"]) * 2 / np.pi
            x = radius * pos["x"] * alpha
            y = radius * pos["y"] * alpha
            if ind % 2 == 0:
                add_coef = 1
                txt = str(int(ind / 2))
                fill = False
                color = "orange"
            else:
                add_coef = 0.2
                txt = ""
                fill = True
                color = day_colors[col_ind]

            ax1.add_patch(mpatches.Circle((x, y), radius * small_coff * add_coef, fill=fill, color=color, alpha = 15))
            ax1.text(x, y, txt, size=6, horizontalalignment= "center", verticalalignment= "center")


ax1.set_title("Latitude: " + str(round(json_dict_1.get("latitude"))) + "°" + ", Longitude: " +
              str(round(json_dict_1.get("longitude"))) + "°" + ", Days: " + str(json_dict_1.get("days")) +
              ",\nIntegral insolation: " + str(round(json_dict_1.get("integral_insolation"), 3)), size=10)
ax1.autoscale()

# ------------------------------------------------  ax_best

# ax_list = [ax_best, ax_worst]
# names = ["Best", "Worst"]
# ind_ax = -1
# for json_dict in [json_dict_2, json_dict_3]:
#     ind_ax += 1
#     contour = list(map(lambda x: list(map(lambda y: list((y["x"], y["y"])), x)), json_dict["buildings"]))
#     max_res = -1
#     ind = -1
#     for contour_i in contour:
#         ind += 1
#         fill = True
#         if ind == 0:
#             fill = False
#         ax_list[ind_ax].add_patch(mpatches.Polygon(np.array(contour_i),  # (x,y)
#                                            color='orange',
#                                             fill=fill,
#                                            linewidth = 1,
#                                            alpha=0.6,
#                                             linestyle = '-'
#                                            ))
#
#     ind = -1
#     for point in json_dict["windows"]:
#         ind += 1
#         ax_list[ind_ax].add_patch(mpatches.Circle(np.array(point), 0.05,  # (x,y)
#                                       color='blue',
#                                       # closed=False,
#                                       fill=True,
#                                       linewidth=0.5,
#                                       alpha=1,
#                                       ))
#
#     if not json_dict.get("insolation_by_wall") is None:
#         ind = -1
#         shift_txt = 0.5
#         for p1 in contour[0]:
#             ind += 1
#
#             p1 = np.array(contour[0][ind])
#
#             ind_next = ind + 1
#             if ind_next > len(contour[0]) - 1:
#                 ind_next = 0
#             p2 = np.array((contour[0])[ind_next])
#             vect = (p2 - p1) / np.sqrt(np.sum((p2 - p1) ** 2))
#             ort_vect = np.array([vect[1], - vect[0]])
#
#             ind_next = ind + 1
#             if ind_next > len(contour[0]) - 1:
#                 ind_next = 0
#             p2 = (contour[0])[ind_next]
#
#             ax_list[ind_ax].text(0.5 * (p1[0] + p2[0]) + shift_txt * ort_vect[0], 0.5 * (p1[1] + p2[1]) + shift_txt * ort_vect[1], str(round(json_dict.get("insolation_by_wall")[ind], 2)), size=6, )
#
#     ax_list[ind_ax].set_title(names[ind_ax] + " solution.\nAverage insolation: " + str(round(json_dict.get("integral_insolation"), 2)) +
#                       ", angle of rotation: " + str(json_dict.get("angle"))+"°", size=10)
#
#     ax_list[ind_ax].autoscale()
#


# ------------------------------------------------ ax_min

# theta = np.arange(0, 360) * np.pi / 180
#
# ax_aver.plot(theta, json_dict_4.get("insolation_by_angle"))
#
# #ax_aver.set_rmax(json_dict_2["integral_insolation"] * 1.1)
# ax_aver.set_rticks([0.1, 0.2, 0.3, 0.4, 0.5])
# ax_aver.set_title("Average insolation depends on angle of rotation", size=9)
# ax_aver.set_theta_zero_location("N")
# ax_aver.set_theta_direction(-1)
#
# ax_min.plot(theta, json_dict_4.get("min_wind_insol_by_angle"))
#
# #ax_aver.set_rmax(json_dict_2["integral_insolation"] * 1.1)
# ax_min.set_rticks([0.1, 0.2, 0.3, 0.4, 0.5])
#
# ax_min.set_title("The darkest window's insolation depends on angle of rotation", size=9)
# ax_min.set_theta_zero_location("N")
# ax_min.set_theta_direction(-1)

#plt.xlabel('axe X')
#plt.ylabel('axe Y')
plt.autoscale()
plt.show()




