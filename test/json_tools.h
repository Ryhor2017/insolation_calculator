//
// Created by ryhor on 11/07/19.
//

#ifndef SUNLIGHT_JSON_TOOLS_H
#define SUNLIGHT_JSON_TOOLS_H

#include "../src/sunlight/input.h"
#include "../src/sunlight/solver.h"

#include "../third_party/json/json.hpp"
#include "../third_party/json/jsut.h"

void CreateVisualJSON(const sun::Input &input, const std::string &file_name);

void CreateVisualJSON(const sun::Solution &solution, const sun::Input &input, const std::string &file_name);

int ReadJSONInput(const std::string &file_name, sun::Input &input, bool read_binary);

#endif //SUNLIGHT_JSON_TOOLS_H
